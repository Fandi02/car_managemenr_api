var express = require("express");
var router = express.Router();
const Validator = require("fastest-validator");

const { Car } = require("../models");

const v = new Validator();

router.get("/", async (req, res) => {
  const cars = await Car.findAll();
  return res.json(cars);
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;
  const car = await Car.findByPk(id);
  return res.json(car || {});
});

router.post("/addCar", async (req, res) => {
  const schema = {
    name_car: "string",
    price: "string",
    image: "string",
    size: "string",
  };

  const validate = v.validate(req.body, schema);

  if (validate.length) {
    return res.status(400).json({ validate });
  }

  const car = await Car.create(req.body);

  res.json(car);
});

router.put("/updateCar/:id", async (req, res) => {
  const id = req.params.id;

  let car = await Car.findByPk(id);

  if (!car) {
    return res.status(404).json({
      message: "Car not found",
    });
  }

  const schema = {
    name_car: "string|optional",
    price: "string|optional",
    image: "string|optional",
    size: "string|optional",
  };

  const validate = v.validate(req.body, schema);

  if (validate.length) {
    return res.status(400).json({ validate });
  }

  car = await car.update(req.body);

  res.json(car);
});

router.delete("/deleteCar/:id", async (req, res) => {
  const id = req.params.id;

  const car = await Car.findByPk(id);

  if (!car) {
    return res.status(404).json({
      message: "Car not found",
    });
  }

  await car.destroy();

  res.json({
    message: "Car deleted",
  });
});

module.exports = router;
